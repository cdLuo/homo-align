from utils.std_align import lcs_align

def realign_let(norm, tok):
    '''The implementation of letter-level alignment'''
    freq_comb = ['ck', 'ey', 'ie', 'ou', 'wh']
    tok = tok.lower()
    norm = norm.lower()
    ops = lcs_align(norm, tok, True)
    # print (ops)
    ops.append('END')
    parsed = []
    chunk_d = []
    chunk_i = []
    for op in ops:
        if op[1] == '-':
            chunk_d.append(op)
        elif op[0] == '+':
            chunk_i.append(op)
        elif op[0] == op[1] or op == 'END':
            if not chunk_d and not chunk_i:
                parsed.append(op)
            elif chunk_d and not chunk_i:
                parsed = parsed + chunk_d
                parsed.append(op)
                chunk_d = []
            elif not chunk_d and chunk_i:
                i_let = chunk_i[0][1]
                if op == 'END':
                    if parsed:
                        prev_op = parsed[-1]
                        comb = prev_op[1]+i_let
                        if comb in freq_comb:
                            parsed[-1] = (prev_op[0], comb)
                            parsed.append(op)
                            if len(chunk_i) != 1:
                                return []
                        else:
                            return []
                    else:
                        return []
                elif len(chunk_i) == 1:
                    if parsed:
                        prev_op = parsed[-1]
                        comb = prev_op[1]+i_let
                        if comb in freq_comb:
                            parsed[-1] = (prev_op[0], comb)
                            parsed.append(op)
                        else:
                            parsed.append((op[0], i_let+op[1]))
                    else:
                        parsed.append((op[0], i_let+op[1]))
                else:
                    i_rest = ''.join([insert[1] for insert in chunk_i[1:]])
                    if parsed:
                        prev_op = parsed[-1]
                        comb = prev_op[1]+i_let
                        if comb in freq_comb:
                            parsed[-1] = (prev_op[0], comb)
                            parsed.append((op[0], i_rest+op[1]))
                        else:
                            parsed.append((op[0], i_let+i_rest+op[1]))
                    else:
                        parsed.append((op[0], i_let+i_rest+op[1]))
                chunk_i = []

            elif chunk_d and chunk_i:
                i_let = chunk_i[0][1]
                if len(chunk_i) == 1:
                    if parsed:
                        prev_op = parsed[-1]
                        comb = prev_op[1]+i_let
                        if comb in freq_comb:
                            parsed[-1] = (prev_op[0], comb)
                            parsed = parsed + chunk_d

                        else:
                            parsed.append((''.join([delete[0] for delete in chunk_d]), i_let))
                    else:
                        parsed.append((''.join([delete[0] for delete in chunk_d]), i_let))

                else:
                    i_rest = ''.join([insert[1] for insert in chunk_i[1:]])
                    if parsed:
                        prev_op = parsed[-1]
                        comb = prev_op[1]+i_let
                        if comb in freq_comb:
                            parsed[-1] = (prev_op[0], comb)
                            parsed.append((''.join([delete[0] for delete in chunk_d]), i_rest))
                        else:
                            parsed.append((''.join([delete[0] for delete in chunk_d]), i_let+i_rest))
                    else:
                        parsed.append((''.join([delete[0] for delete in chunk_d]), i_let+i_rest))
                parsed.append(op)
                chunk_d = []
                chunk_i = []
    parsed = parsed[:-1]
    # print(parsed)
    return parsed

if __name__ == '__main__':
    w1,w2 = "long", "looong"
    print (realign_let(w1,w2))
