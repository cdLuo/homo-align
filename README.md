# README #
This is the implementation of the proposed method (homo-align) in the submitted paper, *Recognizing Phonetic Substitutions in Non-standard Words for Text Normalization*.

### Requirements
* Python3
* numpy

### Description
We upload the code and data used in the **Intrinsic evaluation** in the paper, so that it can be easily reproduced.

* The gold standard alignments are in `gold.align`.
* We reimplement the letter-level alignment method in `let_align.py`.
* The alignments produced by GIZA++ are in `giza_viterbi.align`.
* The proposed method is implemented in `homo_align.py`, and the homophone knowledge is automatically extracted from the L2P alignments `whole.lex.m-mAlign.2-2.delX.1-best.conYX.align`

### Intrinsic evaluation
Run `evaluate.py` to check the evaluation results.
