from homo_align import realign_homo
from utils.format import giza_to_L2L, homo_to_L2L
from let_align import realign_let


# reading gold.align
gold = []
p = []
with open('gold.align') as in_gold:
    for line in in_gold.readlines():
        pair = line.strip().split('\t')
        if line == '\n':
            if p:
                gold.append(p)
            continue
        if len(pair) == 2:
            p = [pair,[]]
        else:
            op = line.strip().split(',')
            if len(op) != 3:
                raise Exception('error format: {}'.format(op))
            g = op[0]
            gg = op[1]
            t = op[2]
            p[1].append((g,gg,t))
# print (len(gold))

gold_aligns = []
for pair,ops in gold:
    g_ops = []
    for g,gg,op in ops:
        g_ops.append((g,gg))
    gold_aligns.append(g_ops)
# print (len(gold_aligns))

giza = giza_to_L2L('giza_viterbi.align')
# print (len(giza))

homo = []
let = []
for pair,ops in gold:
    w,nsw = pair

    homo_align = homo_to_L2L(realign_homo(w,nsw))
    homo.append(homo_align)

    let_align = []
    for g,gg in realign_let(w,nsw):
        let_align.append((g,gg))
    let.append(let_align)
# print (len(homo), len(let))

ic_let = []
ic_homo = []
ic_stat = []
for i in range(len(gold_aligns)):
    gold_align = gold_aligns[i]
    let_align = let[i]
    homo_align = homo[i]
    stat_align = giza[i]
    if let_align != gold_align:
        ic_let.append((i,let_align))
    if homo_align != gold_align:
        ic_homo.append((i,homo_align))
    if stat_align != gold_align:
        ic_stat.append((i,stat_align))

total = len(gold_aligns)
let_acc = round(float(total - len(ic_let))/total, 4)
giza_acc = round(float(total - len(ic_stat))/total, 4)
homo_acc = round(float(total - len(ic_homo))/total, 4)

print ('*'*20)
print ("Let-align: {}".format(let_acc))
print ("GIZA-align: {}".format(giza_acc))
print ("Homo-align: {}".format(homo_acc))
print ('*'*20)
