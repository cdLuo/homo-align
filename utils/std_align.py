import numpy as np

def ed_dist(s,t):
    m = len(s)+1
    n = len(t)+1
    if len(s) == 0 or len(t) == 0:
        return []
    d = np.zeros((m,n))
    reverse = False
    if s.endswith(t) or t.endswith(s):
        s = s[::-1]
        t = t[::-1]
        reverse = True
    for i in range(m):
        d[i][0] = i
    for j in range(n):
        d[0][j] = j
    for i in (range(1,m)):
        for j in range(1,n):
            if s[i-1] == t[j-1]:
                c = 0
            else: #substitution
                c = 1
            d[i][j] = min(
                d[i-1][j]+1,
                d[i][j-1]+1,
                d[i-1][j-1]+c,
            )
    return d[-1][-1]

def ed_align(s,t):
    m = len(s)+1
    n = len(t)+1
    if len(s) == 0 or len(t) == 0:
        return []
    d = np.zeros((m,n))
    reverse = False
    if s.endswith(t) or t.endswith(s):
        s = s[::-1]
        t = t[::-1]
        reverse = True
    #print s,t

    for i in range(m):
        d[i][0] = i
    for j in range(n):
        d[0][j] = j
    for i in (range(1,m)):
        for j in range(1,n):
            if s[i-1] == t[j-1]:
                c = 0
            else: #substitution
                c = 1
            d[i][j] = min(
                d[i-1][j]+1,
                d[i][j-1]+1,
                d[i-1][j-1]+c,
            )
    # print (d)
    i = m-1
    j = n-1
    str_align = []
    while i != 0 or j != 0:
        if i == 0:
            str_align.append(('',t[j-1]))
            j -= 1
        elif j == 0:
            str_align.append((s[i-1],''))
            i -= 1
        else:
            if d[i-1][j-1] == min(d[i-1][j-1],
                                  d[i-1][j],
                                  d[i][j-1]):
                str_align.append((s[i-1],t[j-1]))
                i -= 1
                j -= 1
            elif d[i-1][j] == min(d[i-1][j-1],
                                  d[i-1][j],
                                  d[i][j-1]):
                str_align.append((s[i-1],''))
                i -= 1
            elif d[i][j-1] == min(d[i-1][j-1],
                                  d[i-1][j],
                                  d[i][j-1]):
                str_align.append(('',t[j-1]))
                j -= 1
    if reverse:
        return str_align
    return list(reversed(str_align))

def lcs_align(s,t, del_first=False):
    m = len(s)+1
    n = len(t)+1
    d = np.zeros((m,n))
    reverse = False
    if s.endswith(t) or t.endswith(s):
        reverse = True
    else:
        s = s[::-1]
        t = t[::-1]
    for i in range(1,m):
        for j in range(1,n):
            if s[i-1] == t[j-1]:
                d[i][j] = d[i-1][j-1] + 1
            else:
                d[i][j] = max(d[i-1][j],d[i][j-1])
    # print (d)
    if del_first:
        if reverse:
            return lcs_del(d, s, t, len(s), len(t))[::-1]
        return lcs_del(d, s, t, len(s), len(t))
    if reverse:
        return lcs_ins(d, s, t, len(s), len(t))[::-1]
    return lcs_ins(d, s, t, len(s), len(t))

def lcs_str(s,t, del_first=False): # Return subsequence of LCS
    align = lcs_align(s,t, del_first)
    lcstr = ''
    for a,b in align:
        if a == b:
            lcstr += a
    return lcstr

def lcs_del(d, s, t, i, j): # Deletion first LCS
    result = []
    if i > 0 and j > 0 and s[i-1] == t[j-1]:
        result.append((s[i-1], s[i-1]))
        result += lcs_del(d, s, t, i-1, j-1)
    elif i > 0 and (j == 0 or d[i][j-1] <= d[i-1][j]):
        result.append((s[i-1], '-'))
        result += lcs_del(d, s, t, i-1, j)
    elif j > 0 and (i == 0 or d[i][j-1] >= d[i-1][j]):
        result.append(('+', t[j-1]))
        result += lcs_del(d, s, t, i, j-1)
    return result

def lcs_ins(d, s, t, i, j): # Insertion first LCS
    result = []
    if i > 0 and j > 0 and s[i-1] == t[j-1]:
        result.append((s[i-1], s[i-1]))
        result += lcs_ins(d, s, t, i-1, j-1)
    elif j > 0 and (i == 0 or d[i][j-1] >= d[i-1][j]):
        result.append(('+', t[j-1]))
        result += lcs_ins(d, s, t, i, j-1)
    elif i > 0 and (j == 0 or d[i][j-1] <= d[i-1][j]):
        result.append((s[i-1], '-'))
        result += lcs_ins(d, s, t, i-1, j)
    return result

def lcs_length(s,t): # Return length of LCS
    d = np.zeros((len(s)+1,len(t)+1))
    for i in range(1,len(s)+1):
        for j in range(1,len(t)+1):
            if s[i-1] == t[j-1]:
                d[i][j] = d[i-1][j-1] + 1
            else:
                d[i][j] = max(d[i][j-1],d[i-1][j])
    return d[-1, -1]

if __name__ == '__main__':
    w1,w2 = 'applewhite','appli'
    print(ed_dist(w1,w2))
    print(lcs_length(w1,w2))
    print(lcs_str(w1,w2))
