import re

def giza_to_L2L(fp):

    p1 = "[\(\)\{\}]+"
    p2 = "[a-zA-z']+"
    stat_aligned = []
    with open(fp) as in_file:
        lines = in_file.readlines()
        for i in range(0,len(lines),3):
            nsw = lines[i+1].strip().split(' ')
            items = lines[i+2].strip().split(' ')
            lst = []
            pair = []
            for item in items:
                if re.match(p1,item):
                    continue
                if re.match(p2,item):
                    if pair:
                        lst.append(pair)
                    pair = [item]
                else:
                    pair.append(item)
            lst.append(pair)
            null_item = lst[0]
            ins_inds = null_item[1:]
            ins_inds.reverse()
            cur_ins = 0
            if ins_inds:
                cur_ins = int(ins_inds.pop())
            cur_ind = 1
            new_lst = []
            for item in lst[1:]:
                let = item[0]
                inds = item[1:]
                if not inds:
                    new_lst.append((let,'-'))
                else:
                    ind = inds[0]
                    more_ins = inds[1:]
    #                 print cur_ind,ind
                    if cur_ins == cur_ind:
                        new_lst.append(('+',nsw[cur_ind-1]))
                        cur_ind += 1
                        if ins_inds:
                            cur_ins = int(ins_inds.pop())
                        else:
                            cur_ins = 0
                    if int(ind) == cur_ind:
                        new_lst.append((let, nsw[cur_ind-1]))
                        cur_ind += 1
                    if more_ins:
                        start = int(more_ins[0])
                        end = int(more_ins[-1])
    #                     print start,end
                        if len(more_ins) == 1:
                            new_lst.append(('+',nsw[cur_ind-1]))
                            cur_ind += 1
                        else:
                            ins_seq = ''.join(nsw[start-1:end])
                            new_lst.append(('+',ins_seq))
                            cur_ind = end+1
            while cur_ins == cur_ind:
                new_lst.append(('+',nsw[cur_ind-1]))
                cur_ind += 1
                if ins_inds:
                    cur_ins = int(ins_inds.pop())
                else:
                    cur_ins = 0
            stat_aligned.append(new_lst)
    return stat_aligned

def homo_to_L2L(align):
    result = []
    for p,g,gg,op in align:
        if len(op) == 1:
            result.append((g,gg))
        elif len(g) == len(gg):
            for i in range(len(g)):
                result.append((g[i],gg[i]))
        elif 'S' in op:
            result.append((g,gg))
        elif op == 'NRN':
            segs = gg.split('|')
            for i in range(len(segs)):
                result.append((g[i],segs[i]))
    return result


if __name__ == '__main__':
    print (len(giza_to_L2L('../giza_viterbi.align')))
