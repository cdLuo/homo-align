import re
# import cmudict

def get_dict(dp):
    homo_dict = dict() # homophone dictionary
    poly_dict = dict() # polyphone dictionary, not used for now.
    cmu_dict = dict() # L2P alignment of CMU dict
    with open(dp) as fr:
        for line in fr.readlines():
            g,p = line.strip().split('\t')
            w = re.sub('[|:]', '', g)
            g_group = g.split('|')[:-1]
            p_group = p.split('|')[:-1]
            unit_pairs = list(zip(g_group,p_group))
            for i,(graph, phone) in enumerate(unit_pairs):
                if phone not in homo_dict:
                    homo_dict[phone] = set([graph])
                else:
                    homo_dict[phone].add(graph)
                if graph not in poly_dict:
                    poly_dict[graph] = set([phone])
                else:
                    poly_dict[graph].add(phone)
            cmu_dict[w] = unit_pairs
        return homo_dict, cmu_dict

if __name__ == '__main__':
    print(len(get_homo_dict('../stress.lex.m-mAlign.2-2.delX.1-best.conYX.align')))
