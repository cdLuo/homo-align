# -*- coding: utf-8 -*-

import re

from utils.dict import get_dict
from utils.std_align import lcs_align

homo_dict, cmu_dict = get_dict('whole.lex.m-mAlign.2-2.delX.1-best.conYX.align')

similar_sound = {
    'DH':['D'],
    'AH':['AA', 'UH'],
    'ER':['AA'],
    'AA':['AH', 'AO']
}

special = {
    '4':'for',
    '8':'eit',
    '2':'to',
    '1':'one',
    '@':'at',
    '&':'and',
}

key_board = {
    'q':['w'],
    'w':['q','e'],
    'e':['w','r'],
    'r':['e','t'],
    't':['r','y'],
    'y':['t','u'],
    'u':['y','i'],
    'i':['u','o'],
    'o':['i','p', '0'],
    'p':['o'],
    'a':['s', '4'],
    's':['a','d', '5'],
    'd':['s','f'],
    'f':['d','g'],
    'g':['f','h', 'q'],
    'h':['g','j'],
    'j':['h','k'],
    'k':['j','l'],
    'l':['k'],
    'z':['x'],
    'x':['z','c'],
    'c':['x','v'],
    'v':['c','b'],
    'b':['v','n'],
    'n':['b','m'],
    'm':['n']
}

def get_op_seq(w,vw, del_first=False):
    ops = lcs_align(w,vw, del_first)
    seq = ''
    for s,t in ops:
        if s == t:
            seq += 'N'
        elif s == '+':
            seq += 'I'
        elif t == '-':
            seq += 'D'
    return seq

def pre_align(ops, seq, del_first=False): # Pre-alignment for LCS alignments
    trans_p = "IND"
    rep_p = "I+"
    key_p = "(I+)(D+)"
    if del_first:
        trans_p = "DNI"
        key_p = "(D+)(I+)"
    # handle letter transposition
    rest_seq = seq
    rest_ops = ops
    new_seq = ''
    new_ops = []
    while re.search(trans_p, rest_seq):
        m = re.search(trans_p, rest_seq)
        start = m.start()
        end = m.end()
        if del_first:
            D_op = rest_ops[start]
            mid_op = rest_ops[start+1]
            I_op = rest_ops[end-1]
            if I_op[1] == D_op[0]:
                sub_seq = "NT"
                sub_ops = [(D_op[0], I_op[1]), mid_op]
                new_seq += rest_seq[:start] + sub_seq
                new_ops += rest_ops[:start] + sub_ops
            else:
                new_seq += rest_seq[:end]
                new_ops += rest_ops[:end]
        else:
            I_op = rest_ops[start]
            mid_op = rest_ops[start+1]
            D_op = rest_ops[end-1]
            if I_op[1] == D_op[0]:
                sub_seq = "NT"
                sub_ops = [mid_op, (D_op[0], I_op[1])]
                new_seq += rest_seq[:start] + sub_seq
                new_ops += rest_ops[:start] + sub_ops
            else:
                new_seq += rest_seq[:end]
                new_ops += rest_ops[:end]
        rest_seq = rest_seq[end:]
        rest_ops = rest_ops[end:]
    new_seq += rest_seq
    new_ops += rest_ops
    # handle letter repetition
    rest_seq = new_seq
    rest_ops = new_ops
    new_seq = ''
    new_ops = []
    while re.search(rep_p, rest_seq):
        m = re.search(rep_p, rest_seq)
        start = m.start()
        end = m.end()
        j = 1
        rep_char = ''
        while start-j >= 0 and rest_ops[start-j][1] == '-':
            j += 1
        if start-j >= 0:
            rep_char = rest_ops[start-j][1]
            left_seq = rest_seq[:start]
            left_ops = rest_ops[:start]
            right_seq = rest_seq[end:]
            right_ops = rest_ops[end:]
        elif start == 0:
            j = 0
            while end+j < len(rest_ops) and rest_ops[end+j][1] == '-':
                j += 1
            if end+j < len(rest_ops):
                rep_char = rest_ops[end+j][1]
                left_seq = rest_seq[end:end+j+1]
                left_ops = rest_ops[end:end+j+1]
                right_seq = rest_seq[end+j+1:]
                right_ops = rest_ops[end+j+1:]
        is_rep = True
        rep_str = ''
        for plus,char in rest_ops[start:end]:
            if char != rep_char:
                is_rep = False
                break
            rep_str += char
        if is_rep:
            sub_seq = 'R'
            sub_ops = [('+', rep_str)]
            new_seq += left_seq + sub_seq
            new_ops += left_ops + sub_ops
            rest_seq = right_seq
            rest_ops = right_ops
        else:
            new_seq += rest_seq[:end]
            new_ops += rest_ops[:end]
            rest_seq = rest_seq[end:]
            rest_ops = rest_ops[end:]
    new_seq += rest_seq
    new_ops += rest_ops
    # print (new_seq, new_ops)
    # handle keyboard substitution
    rest_seq = new_seq
    rest_ops = new_ops
    new_seq = ''
    new_ops = []
    while re.search(key_p, rest_seq):
        m = re.search(key_p, rest_seq)
        if del_first:
            start_d,end_d = m.span(1)
            start_i,end_i = m.span(2)
            sub_seq = ''
            sub_ops = []
            j = 0
            for i,(char,minus) in enumerate(rest_ops[start_d:end_d]):
                d_pos = start_d + i
                i_pos = start_i + j
                if i_pos < end_i:
                    plus, i_char = rest_ops[i_pos]
                else:
                    i_char = '#'
                if char in key_board:
                    if i_char in key_board[char]:
                        sub_seq += 'K'
                        sub_ops += [(char, i_char)]
                        j += 1
                        continue
                sub_seq += 'D'
                sub_ops += [(char, minus)]
            sub_seq += rest_seq[start_i+j:end_i]
            sub_ops += rest_ops[start_i+j:end_i]
            new_seq += rest_seq[:start_d] + sub_seq
            new_ops += rest_ops[:start_d] + sub_ops
            rest_seq = rest_seq[end_i:]
            rest_ops = rest_ops[end_i:]
        else:
            start_i,end_i = m.span(1)
            start_d,end_d = m.span(2)
            sub_seq = ''
            sub_ops = []
            j = 0
            for i,(plus,char) in enumerate(rest_ops[start_i:end_i]):
                i_pos = start+i
                d_pos = start_d+j
                if d_pos < end_d:
                    d_char, minus = rest_ops[d_pos]
                else:
                    d_char = '#'
                if d_char in key_board:
                    if char in key_board[d_char]:
                        sub_seq += 'K'
                        sub_ops += [(d_char, char)]
                        j += 1
                        continue
                sub_seq += 'I'
                sub_ops += [(plus, char)]
            sub_seq += rest_seq[start_d+j:end_d]
            sub_ops += rest_ops[start_d+j:end_d]
            new_seq += rest_seq[:start_i] + sub_seq
            new_ops += rest_ops[:start_i] + sub_ops
            rest_seq = rest_seq[end_d:]
            rest_ops = rest_ops[end_d:]
    new_seq += rest_seq
    new_ops += rest_ops

    return new_ops, new_seq

def print_all_k_length(chars, k):
    # Enumerate all possible partitions
    div_seqs = []
    print_all_k_length_rec(chars, '', k, div_seqs)
    return div_seqs

def print_all_k_length_rec(chars, prefix, k, div_seqs):
    if k == 0:
        div_seqs.append(prefix)
        return
    for i in range(len(chars)):
        new_prefix = ''
        new_prefix = prefix + chars[i]
        print_all_k_length_rec(chars, new_prefix, k-1, div_seqs)

def enumerate_segs(seq):
    # Enumerate all legal partitions
    n = len(seq)
    if n == 0:
        return []
    divs = print_all_k_length(['0','1'], n-1)
    valid_divs = []
    for div in divs:
        if '00' not in div:
            valid_divs.append(div)
    segs = []
    for div in valid_divs:
        seg = [seq[0]]
        for i,char in enumerate(div):
            if char == '1':
                seg.append('|')
                seg.append(seq[i+1])
            else:
                seg.append(seq[i+1])
        a = []
        item = ''
        prev_i = 0
        for i,ele in enumerate(seg):
            if ele == '|':
                a.append(seg[prev_i:i])
                prev_i = i+1
        a.append(seg[prev_i:])
        segs.append(a)
    return segs

def match(i_seg, d_seg):
    # Homophone match process
    # print (i_seg,d_seg)
    result = []
    i = 0
    j = 0
    while i < len(i_seg):
        grapheme = ':'.join(i_seg[i])
        is_match = False
        # print (grapheme)
        while j < len(d_seg):
            # print (d_seg[j])
            phoneme = ':'.join([ph for ph,lets in d_seg[j]])
            d_lets = ''.join([''.join(lets) for ph,lets in d_seg[j]])
            if (phoneme in homo_dict and grapheme in homo_dict[phoneme] and '_' not in phoneme) or (grapheme == d_lets):
                is_match = True
                result.append((phoneme, d_lets,''.join(i_seg[i]), 'S'))
                j += 1
                break
            elif phoneme in similar_sound:
                sim_phonemes = similar_sound[phoneme]
                for sim_ph in sim_phonemes:
                    if sim_ph in homo_dict and grapheme in homo_dict[sim_ph] and '_' not in sim_ph:
                        is_match = True
                        result.append((phoneme, d_lets, ''.join(i_seg[i]), 'S'))
                        j += 1
                        break
                if is_match:
                    break
                else:
                    result.append((phoneme, d_lets, '-'*len(d_lets), 'D'*len(d_lets)))
                    j += 1
            else:
                result.append((phoneme, d_lets, '-'*len(d_lets), 'D'*len(d_lets)))
                j += 1
        if not is_match:
            return []
        i += 1
    if j < len(d_seg):
        for seg in d_seg[j:]:
            phoneme = ':'.join([ph for ph,lets in seg])
            d_lets = ''.join([''.join(lets) for ph,lets in seg])
            result.append((phoneme, d_lets, '-'*len(d_lets), 'D'*len(d_lets)))
    # print ("segmentation: ", i_seg, d_seg)
    # print ("alignment: ", result)
    return result

def parse_chunk(chunk, prev_op, next_op):
    # Process each chunk to recognize phonetic substitutions
    chunk_d = []
    chunk_i = ''
    special_pair = ()
    has_ins =  False
    # print(chunk)
    for ph, ops, op_types in chunk:
        if ph != '?':
            d_str = ''
            for op in ops:
                if op[0] != '+':
                    d_str += op[0]
                if op[1] != '-':
                    chunk_i += op[1]
                if op[0] == '+':
                    has_ins = True
            chunk_d.append((ph, d_str))
        else:
            has_ins = True
            assert(len(ops) == 1)
            i_let = ops[0][1]
            if i_let in special:
                chunk_i += special[i_let]
                special_pair = (i_let, special[i_let])
            else:
                chunk_i += ops[0][1]
    # print(chunk_d, chunk_i)
    if not has_ins:
        result = []
        for ph_op in chunk:
            ph = ph_op[0]
            l_ops = ph_op[1]
            d_str = ''.join([d for d,i in l_ops])
            i_str = ''.join([i for d,i in l_ops])
            op_types = ph_op[2]
            result.append((ph, d_str, i_str, op_types))
        if prev_op:
            return [prev_op] + result + [next_op]
        return result + [next_op]
    if not chunk_i:
        result = []
        for ph, d_str in chunk_d:
            result.append((ph, d_str, '-'*len(d_str), 'D'*len(d_str)))
        if prev_op:
            return [prev_op] + result + [next_op]
        return result + [next_op]
    if not chunk_d:
        if len(chunk_i) == 1:
            if prev_op and len(prev_op[2]) == 1:
                prev_ph, d_str, i_str, op_types = prev_op
                g = i_str + ':' + chunk_i
                if prev_ph in homo_dict and g in homo_dict[prev_ph] and '_' not in prev_ph:
                    return [(prev_ph, d_str, re.sub(':', '', g), 'S'), next_op]
            if len(next_op[2]) == 1:
                next_ph, d_str, i_str, op_types = next_op
                g = chunk_i + ':' + i_str
                if next_ph in homo_dict and g in homo_dict[next_ph] and '_' not in next_ph:
                    if prev_op:
                        return [prev_op, (next_ph, d_str, re.sub(':', '', g), 'S')]
                    return [(next_ph, d_str, re.sub(':', '', g), 'S')]
        return []
    i_segs = enumerate_segs(chunk_i)
    d_segs = enumerate_segs(chunk_d)
    # print(i_segs)
    # print(d_segs)
    best_length = len(chunk_d)
    best_match = []
    for i_seg in i_segs:
        for d_seg in d_segs:
            match_result = match(i_seg, d_seg)
            if match_result:
                if abs(best_length-len(best_match)) > abs(best_length-len(match_result)):
                    best_match = match_result
    # print(best_match)
    if not best_match:
        return []
    if special_pair:
        sp_let, sp_str = special_pair
        start_i = 0
        end_i = 0
        for i,item in enumerate(best_match):
            ph,s,t,op_type = item
            if sp_str.startswith(t):
                start_i = i
            if sp_str.endswith(t):
                end_i = i
        if start_i < end_i:
            result = []
            result += best_match[:start_i]
            phs = ':'.join([ph for ph,s,t,op_type in best_match[start_i:end_i+1]])
            lets = ''.join([s for ph,s,t,op_type in best_match[start_i:end_i+1]])
            result.append((phs,lets,sp_let, 'S'))
            result += best_match[end_i+1:]
            if prev_op:
                return [prev_op] + result + [next_op]
            return result + [next_op]
        else:
            return []
    else:
        if prev_op:
            return [prev_op] + best_match + [next_op]
        return best_match + [next_op]

def realign_ph(norm, tok, del_first=False):
    tok = tok.lower()
    norm = norm.lower()
    ops = lcs_align(norm, tok, del_first)
    seq = get_op_seq(norm, tok, del_first)
    # print (ops,seq)
    ops,seq = pre_align(ops, seq, del_first)
    # print (ops,seq)
    if norm not in cmu_dict:
        return []
    unit_pairs = cmu_dict[norm]
    ph_align = []
    i = 0
    j = 0
    while i < len(unit_pairs) or j < len(ops):
        if i < len(unit_pairs):
            g,p = unit_pairs[i]
        if j < len(ops):
            op = ops[j]
            op_type = seq[j]
        if op[0] == '+':
            ph_align.append(('?', [op], op_type))
            j += 1
        else:
            d_lets = g.split(':')
            ph_ops = []
            op_types = ''
            k = 0
            while op[0] == d_lets[k] or op[0] == '+':
                if op[0] == '+':
                    ph_ops.append(op)
                    op_types += op_type
                    j += 1
                    if j >= len(ops):
                        break
                else:
                    ph_ops.append(op)
                    op_types += op_type
                    j += 1
                    k += 1
                    if j >= len(ops) or k >= len(d_lets):
                        break
                op = ops[j]
                op_type = seq[j]
            if k != len(d_lets):
                raise Exception
            ph_align.append((p, ph_ops, op_types))
            i += 1
    ph_align.append(('', [], '#'))
    # print(ph_align)
    parsed = []
    chunk = []
    for ph_op in ph_align:
        ph = ph_op[0]
        l_ops = ph_op[1]
        d_str = ''.join([d for d,i in l_ops])
        i_str = ''.join([i for d,i in l_ops])
        op_types = ph_op[2]
        if op_types == 'NRN':
            i_str = l_ops[0][1] + '|' + l_ops[1][1] + '|' + l_ops[2][1]
        cur_op = (ph, d_str, i_str, op_types)
        if 'D' in op_types or 'I' in op_types:
            '''store the alignment (containing deletions or insertions) into chunk'''
            chunk.append(ph_op)
        else:
            if not chunk:
                parsed.append(cur_op)
            else:
                # print(chunk)
                result = []
                if parsed:
                    result = parse_chunk(chunk, parsed.pop(), cur_op)
                else:
                    result = parse_chunk(chunk, None, cur_op)
                if not result:
                    return []
                parsed += result
                chunk = []
    return parsed[:-1]

def realign_homo(norm, tok):
    # Process both deletion- and insertion-first LCS alignment
    del_first = True
    ins_first = False
    del_first_alignments = realign_ph(norm, tok, del_first)
    # print(del_first_alignments)
    if del_first_alignments:
        return del_first_alignments
    ins_first_alignments = realign_ph(norm, tok, ins_first)
    # print(ins_first_alignments)
    return ins_first_alignments

if __name__ == "__main__":
    w1,w2 = "because", "caz"
    print (realign_homo(w1,w2))
